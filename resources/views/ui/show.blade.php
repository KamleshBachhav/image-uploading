<!DOCTYPE html>
<html>
<head>
	<title>Upload Image</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="Container">
		<div class="col-md-8" align="center">
			<h2>Record Details</h2>
			<table class="table table-striped table-bordered">
				<tr style="text-align: center;">
					<th>Number</th>
					<th>Title</th>
					<th>Profile</th>
				</tr>
				@foreach($image as $img)
				<tr style="text-align: center;">
					<td>{{ $img->number }}</td>
					<td>{{ $img->title }}</td>
					<td><img src="{{Storage::url('img/'. $img->image)}}" style="height: 70px; width: 70px; border-radius: 50px;"></td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</body>
</html>