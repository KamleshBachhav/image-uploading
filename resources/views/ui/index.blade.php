<!DOCTYPE html>
<html>
<head>
	<title> Upload Image </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row col-md-6" >
			<form enctype="multipart/form-data" action="/store" method="post">
				@csrf <!-- {{ csrf_field() }} -->
				<h2> Fill the Details </h2><br />
				<div class="form-group">
					<label> Number </label>
					<input type="number" name="number" class="form-control" required autofocus>
				</div><br />
				<div class="form-group">
					<label> Title </label>
					<input type="text" name="title" class="form-control" required autofocus>
				</div><br />
				<div class="form-group">
					<label> Image </label>
					<input type="file" name="img" required autofocus>
				</div><br />
				<div class="form-group">
					<input type="submit"  class="btn btn-primary" > 
					<a href="/show" class="btn btn-primary" style="float: right;" > Details </a>  
				</div>
			</form>
		</div>
	</div>
</body>
</html>